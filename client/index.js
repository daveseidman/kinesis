import { Hands } from '@mediapipe/hands/hands.js';
import { Camera } from '@mediapipe/camera_utils/camera_utils.js';
// import { drawConnectors, drawLandmarks, lerp } from '@MediaPipe/drawing_utils/drawing_utils';
import { addEl, createEl } from 'lmnt';
import './index.scss';

const videoElement = document.getElementsByClassName('input_video')[0];

const thumb = { x: 0, y: 0, z: 0 };
const pointer = { x: 0, y: 0, z: 0 };
let pinching = false;
const pinchThreshold = 0.1;
const distance = (a, b) => Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2) + Math.pow(a.z - b.z, 2));
const middle = (a, b) => ({ x: (a.x + b.x) / 2, y: (a.y + b.y) / 2, z: (a.z + b.z) / 2 });
const cursor = {
  x: 0,
  y: 0,
};
const cursorTarget = { x: 0, y: 0 };

const appEl = createEl('div', { className: 'app' });
const cursorEl = createEl('div', { className: 'app-cursor' });

addEl(appEl, cursorEl);
addEl(appEl);

const update = () => {
  cursor.x += (cursorTarget.x - cursor.x) / 10;
  cursor.y += (cursorTarget.y - cursor.y) / 10;
  cursorEl.style.left = `${100 * cursor.x}%`;
  cursorEl.style.top = `${100 * cursor.y}%`;
  requestAnimationFrame(update);
};

update();

const onResults = (results) => {
  document.body.classList.add('loaded');
  if (results.multiHandedness && results.multiHandedness[0].score < 0.99) return;
  if (results.multiHandLandmarks) {
    thumb.x = results.multiHandLandmarks[0][4].x;
    thumb.y = results.multiHandLandmarks[0][4].y;
    thumb.z = results.multiHandLandmarks[0][4].z;
    pointer.x = results.multiHandLandmarks[0][8].x;
    pointer.y = results.multiHandLandmarks[0][8].y;
    pointer.z = results.multiHandLandmarks[0][8].z;
    const pinchAmount = distance(thumb, pointer);
    if (!pinching && pinchAmount <= pinchThreshold) {
      pinching = true;
      cursorEl.classList.add('pinching');
    }
    if (pinching && pinchAmount > pinchThreshold) {
      pinching = false;
      cursorEl.classList.remove('pinching');
    }
    const thumbPointerMiddle = middle(thumb, pointer);
    cursorTarget.x = thumbPointerMiddle.x;
    cursorTarget.y = thumbPointerMiddle.y;
  }
};

const hands = new Hands({ locateFile: file => `https://cdn.jsdelivr.net/npm/@mediapipe/hands@0.1/${file}` });
hands.setOptions({ selfieMode: true, maxNumHands: 1, minDetectionConfidence: 0.75, minTrackingConfidence: 0.5 });
hands.onResults(onResults);

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await hands.send({ image: videoElement });
  },
  width: 1280,
  height: 720,
});
camera.start();
